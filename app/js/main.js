(function() {
	var doc = $(document),
		win = $(window),
		scrollT = win.scrollTop(),
		dataAttr,
		headerH, footerH, dEnd, totalD;

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();
	function clearClasses() {
		if ( win.width() > 768 ) {
			$('[data-id="' + dataAttr + '"]').removeClass('visible');
		}
	}
	function mobileFullPageToggle() {
		$('[data-open]').click(function() {
			dataAttr = $(this).attr('data-open');
			$('[data-id="' + dataAttr + '"]').addClass('visible');
		})
		$('[data-close]').click(function() {
			$('[data-id="' + dataAttr + '"]').removeClass('visible');
		})
	}
	win.on('load resize scroll', function(e) {
		if ( e.type == 'load' ) {
		}
		if ( e.type == 'resize' ) {
			waitForFinalEvent(function() {
				clearClasses();
			}, 500, "resize")
		}
		if ( e.type == 'scroll' ) {
			scrollT = $(this).scrollTop();
		}
	})
	doc.ready(function() {
		mobileFullPageToggle();
	})
}())