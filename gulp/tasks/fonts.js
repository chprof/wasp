module.exports = function () {
    $.gulp.task("fonts", function () {
        return $.gulp.src("./app/fonts/**/*")
            .pipe($.gulp.dest("./build/fonts/"))
            .pipe($.debug({ "title": "fonts" }))
            .on("end", $.bs.reload);
    });
};