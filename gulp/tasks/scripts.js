module.exports = function() {
    $.gulp.task("scripts", function() {
        return $.gulp.src("./app/js/**/main.js")
            .pipe($.gp.sourcemaps.init())
            // .pipe($.gp.babel({presets: ["@babel/preset-env"]}))
            // .pipe($.gp.uglify())
            // .pipe($.gp.rename({suffix: ".min"}))
            .pipe($.gp.sourcemaps.write("./"))
            .pipe($.gulp.dest("./build/js/"))
            .pipe($.debug({"title": "scripts"}))
            .on("end", $.bs.reload);
    });
};